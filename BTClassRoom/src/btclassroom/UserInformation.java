/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package btclassroom;

/**
 *
 * @author binita
 */
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.RootPaneContainer;
public class UserInformation {
    
    DBUtil dbconnection;
    AccountControler control= new AccountControler(dbconnection);
    String UserName;
    String PassWord;
    String Email;
    String DateOfBirth;
    String Status;
    String NewPassWord;
    String VerifyNewPassWord;
    String NewUserName;
    private ResultSet result;

    UserInformation(String username, String password, String email, String dateofbirth, String status)
    {
        this.UserName=username;
        this.PassWord=password;
        this.Email=email;
        this.DateOfBirth=dateofbirth;
        this.Status=status;

    } 
    UserInformation(String email,String password)
    {
        this.PassWord=password;
        this.Email=email;
    }
    UserInformation(String username)
    {
        this.UserName=username;
    }
     UserInformation(String email,String currentpassword,String newpassword,String verifynewpassword)
    {
        this.PassWord=currentpassword;
        this.Email=email;
        this.NewPassWord=newpassword;
        this.VerifyNewPassWord=verifynewpassword;

    }
     UserInformation(String email,String username,String newusername)
    {
        this.UserName=username;
        this.Email=email;
        this.NewUserName=newusername;


    }



    boolean Signup()
    {
        switch (Status) {
            case "Teacher":
                //System.out.println("done");
                try
                {
                    FileInputStream fstream = new FileInputStream("teacherEmail.txt");
            int flag;
            try (DataInputStream in = new DataInputStream(fstream)) {
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                String strLine;
                flag = 0;
                while ((strLine = br.readLine()) != null)
                {
                   // System.out.println(strLine);
                    if(strLine.equals(Email))
                    {
                        flag=1;
                        String squery= "INSERT INTO virtualclassroom VALUES(?,?,?,?,?)";
                        try
                        {
                            dbconnection.prepstmt=(PreparedStatement)dbconnection.connect.prepareStatement(squery);
                            dbconnection.prepstmt.setString(1, UserName);
                            dbconnection.prepstmt.setString(2, PassWord);
                            dbconnection.prepstmt.setString(3, Email);
                            dbconnection.prepstmt.setString(4,DateOfBirth);
                            dbconnection.prepstmt.setString(5, Status);
                            dbconnection.prepstmt.addBatch();
                            dbconnection.prepstmt.executeBatch();
                            new SignUp().setVisible(false);
                            TeacherSignedIn teachersigninpage = new TeacherSignedIn(Email,UserName);
                            teachersigninpage.MAIN();
                            break;
                            
                        }
                        catch (SQLException ex) 
                        {
                            System.out.println("Teacher Signup Error:"+ex);
                        }
                        
                    }
                }
            }
                    if(flag==0)
                    {
                        
                        SignUpError error= new SignUpError();
                        error.MAIN();
                    }
                }
                catch (Exception e)
                {
                    System.err.println("Error: " + e.getMessage());
                }
                break;
            case "Student":
                String squery= "INSERT INTO virtualclassroom VALUES(?,?,?,?,?)";
                try
                {
                    dbconnection.prepstmt=(PreparedStatement)dbconnection.connect.prepareStatement(squery);
                    dbconnection.prepstmt.setString(1, UserName);
                    dbconnection.prepstmt.setString(2, PassWord);
                    dbconnection.prepstmt.setString(3, Email);
                    dbconnection.prepstmt.setString(4,DateOfBirth);
                    dbconnection.prepstmt.setString(5, Status);
                    dbconnection.prepstmt.addBatch();
                    dbconnection.prepstmt.executeBatch();
                    new SignUp().setVisible(false);
                    StudentSignedIn studentsigninpage = new StudentSignedIn(Email,UserName);
                    studentsigninpage.MAIN();
                }
                catch (SQLException ex)
                {
                    Logger.getLogger(UserInformation.class.getName()).log(Level.SEVERE, null, ex);
                    SignUpError error= new SignUpError();
                        error.MAIN();
                }
                break;
        }
            
        
        return true;
    }
    boolean Signin()
    {
        try
        {
            String query="SELECT * FROM virtualclassroom";
            result= dbconnection.stmt.executeQuery(query);
            int flag=0;
            while(result.next())
            {
                String EMAIL= result.getString("Email");
                String PASSWORD=result.getString("PassWord");
                String STATUS= result.getString("Status");
                String USERNAME= result.getString("UserName");
                if(EMAIL.equals(Email) && PASSWORD.equals(PassWord))
                {
                    //System.out.println(STATUS);
                    flag=1;
                    if(STATUS.equals("Teacher"))
                    {
                        
                        new StartUp().setVisible(false);
                        TeacherSignedIn teacher = new TeacherSignedIn(Email,USERNAME);
                        teacher.MAIN();
                        break;
                        //JOptionPane.showMessageDialog(null, "You are a teacher");
                    }
                    if((STATUS.equals("Student")))
                    {
                        new StartUp().setVisible(false);
                        StudentSignedIn studentsigninpage = new StudentSignedIn(Email,USERNAME);
                        studentsigninpage.MAIN();
                        break;
                        //JOptionPane.showMessageDialog(null, "You are a student");
                    }
                    
                    
                }

            }
            if (flag==0)
            {
                new SignUp().setVisible(false);
                SignInError invalidsignininterface = new SignInError();
                invalidsignininterface.MAIN();
                //JOptionPane.showMessageDialog(null, "PASSWPRD OR EMAIL IS INVALID");
                //control.signin();
            }
        }
        catch(SQLException ex)
        {
            Logger.getLogger(UserInformation.class.getName()).log(Level.SEVERE, null, ex);
            //JOptionPane.showMessageDialog(null, "SIGN IN ERROR");
        }
        return true;
    }
    String list(String user) throws SQLException
    {
        String temp="";
        String query="SELECT * FROM virtualclassroom";
            result= dbconnection.stmt.executeQuery(query);
            while(result.next())
            {
                String USERNAME= result.getString("UserName");
                if(!user.equals(USERNAME))
                {
                    temp=temp+USERNAME+"%";
                    System.out.println("here..."+temp);
                }
            }
            return temp;
    }
    boolean Changepassword()
    {

        try {
            String query="SELECT * FROM virtualclassroom";
            result=dbconnection.stmt.executeQuery(query);

            while(result.next())
            {
                String EMAIL=result.getString("Email");
                String PASSWORD=result.getString("PassWord");

                if(EMAIL.equals(Email) && PASSWORD.equals(PassWord))
                {
                    if(NewPassWord.equals(VerifyNewPassWord))
            {
                String squery=" UPDATE virtualclassroom SET PASSWORD= ? WHERE Email= ?";
                try
                {
                    dbconnection.prepstmt= (PreparedStatement)dbconnection.connect.prepareStatement(squery);
                    dbconnection.prepstmt.setString(1, NewPassWord);
                    dbconnection.prepstmt.setString(2, Email);
                    dbconnection.prepstmt.addBatch();
                    dbconnection.prepstmt.executeBatch();
                    JOptionPane.showMessageDialog(null, "Password Changed");
                }
                catch(SQLException ex)
                {
                    Logger.getLogger(UserInformation.class.getName()).log(Level.SEVERE, null, ex);
                    JOptionPane.showMessageDialog(null, "Password not changed");
                }
            }
            else
            {
                JOptionPane.showMessageDialog(null, "passsword not verifiyed");
            }
                    break;
                }
            }
        }
         catch (SQLException ex) {
            Logger.getLogger(UserInformation.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null,"Error in changing password");
        }
        return true;
    }
    boolean Changeusername()
    {
        
        try
        {
            String query="SELECT * FROM virtualclassroom";
            result=dbconnection.stmt.executeQuery(query);
            while(result.next())
            {
                String EMAIL=result.getString("Email");
                if(EMAIL.equals(Email))
                {
                    String squery=" UPDATE virtualclassroom SET UserName= ? WHERE Email= ?";
                    try
                    {
                        dbconnection.prepstmt= (PreparedStatement)dbconnection.connect.prepareStatement(squery);
                        dbconnection.prepstmt.setString(1, NewUserName);
                        dbconnection.prepstmt.setString(2, Email);
                        dbconnection.prepstmt.addBatch();
                        dbconnection.prepstmt.executeBatch();
                        JOptionPane.showMessageDialog(null, "User name Changed");
                    }
                    catch(SQLException ex)
                    {
                        Logger.getLogger(UserInformation.class.getName()).log(Level.SEVERE, null, ex);
                        JOptionPane.showMessageDialog(null, "Password not changed");
                    }
                }

            }
        }
        catch (SQLException ex) {
            Logger.getLogger(UserInformation.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null,"Error in changing password");
        }
        return true;
    }


}



