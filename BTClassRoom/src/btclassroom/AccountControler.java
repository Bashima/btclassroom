/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package btclassroom;

import java.sql.SQLException;

/**
 *
 * @author binita
 */
class AccountControler {
    

    DBUtil dbconnection;

    AccountControler(DBUtil dbconnection) {
        this.dbconnection= dbconnection;
    }
    String list(String username) throws SQLException
    {
        UserInformation newuser=new UserInformation(username);
        newuser.dbconnection=this.dbconnection;
        String temp=newuser.list(username);
        return temp;
    }
    void signup(String username,String password,String email,String dateofbirth,String status)
    {
        switch (status) {
            case "Student":
                {
                    UserInformation newuser= new UserInformation(username,password,email,dateofbirth,status);
                    newuser.dbconnection=this.dbconnection;
                    newuser.Signup();
                    break;
                }
            case "Teacher":
                {
                    UserInformation newuser= new UserInformation(username,password,email,dateofbirth,status);
                    newuser.dbconnection=this.dbconnection;
                    newuser.Signup();
                    break;
                }
        }
    }
    void signin(String email,String password)
    {
        
        
        UserInformation user= new UserInformation(email,password);
        user.dbconnection= this.dbconnection;
        user.Signin();
        
    }
    void changepassword(String email,String currentpassword,String newpassword,String verifypassword)
    {
        
        UserInformation user= new UserInformation(email,currentpassword,newpassword,verifypassword);
        user.dbconnection= this.dbconnection;
        user.Changepassword();
    }
    void changeusername(String email,String username,String newusername)
    {

        UserInformation user= new UserInformation(email,username,newusername);
        user.dbconnection= this.dbconnection;
        user.Changeusername();
    }
}

