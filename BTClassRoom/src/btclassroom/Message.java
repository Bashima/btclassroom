/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package btclassroom;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

/**
 *
 * @author Binita
 */

public final class Message extends JFrame
    

{
	static DefaultListModel list;
        Socket Soc;
        String username;
        String status;
        String email;
        String str[]=new String[20];int namecount;
        Message(String Username,String Status) throws IOException, SQLException
        {
            status=Status;
            username=Username;
            file();
            run();
            read();
        }	
        void read() throws UnknownHostException, IOException
        {
            Soc=new Socket("127.0.0.1",5217);
            DataInputStream din;
            DataOutputStream dout;
            BufferedReader br;
            din = new DataInputStream(Soc.getInputStream());
            dout = new DataOutputStream(Soc.getOutputStream());
            br = new BufferedReader(new InputStreamReader(System.in));
            dout.writeUTF("MREAD");
            String fName=username+".txt";
            dout.writeUTF(fName);
                try {
                        int ch;
                        String temp;
                        String mess="";
                            temp = din.readUTF();
                            //mess=mess+temp;
                            mainText.append(temp);
                            //JOptionPane.showMessageDialog(rootPane, temp);
                    }
                catch (IOException ex) {
                    System.out.println("Error1: "+ex);
                }
               din.close();
                dout.close();
                br.close();
                Soc.close();
                System.out.println("read");
        }
            
        
        void file() throws IOException, SQLException
        {
            
           /* Soc=new Socket("127.0.0.1",5217);
            DataOutputStream dout;
            DataInputStream din;
            dout = new DataOutputStream(Soc.getOutputStream());
            din= new DataInputStream(Soc.getInputStream());
            dout.writeUTF("MUPDATE");
            String temp;
            int i=0;
            
           while(true){
                            temp = din.readUTF();
                            if(temp.equals("##")) {
                   break;
               }
                                str[i]=temp;
                                System.out.println(temp);
                                i++;
                            
                        }
           namecount=i;
           JOptionPane.showMessageDialog(rootPane, username);
            Soc.close();
            din.close();
            dout.close();*/
            DBUtil dbconnection= new DBUtil();
        AccountControler accountcontroler =new AccountControler(dbconnection);
        String temp=accountcontroler.list(username);
        str=temp.split("%");
        namecount=str.length;
            System.out.println("file");
            //System.out.println(str[0]);
        }  
        
	void run()
	{
		          System.out.println("run");
            setTitle("Virtual Classroom Message");
            jScrollPane1 = new javax.swing.JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            jScrollPane2 = new javax.swing.JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            mainText = new javax.swing.JTextArea();
            jScrollPane3 = new javax.swing.JScrollPane(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            jMenuBar1 = new javax.swing.JMenuBar();
            jMenu1 = new javax.swing.JMenu();
            jMenuItem1 = new javax.swing.JMenuItem();
            jMenuItem2 = new javax.swing.JMenuItem();
            list = new DefaultListModel();
            for(int i=0;i<namecount;i++)
            {
                list.addElement(str[i]);
                System.out.println(str[i]);
            }
            nickList = new JList(list);
            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
            nickList.addMouseListener(new java.awt.event.MouseAdapter() 
            {
                @Override
                public void mouseClicked(java.awt.event.MouseEvent evt)    
                {
                    try {
                        nickListMouseClicked(evt);
                    } catch (UnknownHostException ex) {
                        Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            mainText.setColumns(20);
            mainText.setEditable(false);
            mainText.setRows(5);
            mainText.setLineWrap(true);
            jScrollPane2.setViewportView(mainText);
            jScrollPane3.setViewportView(nickList);
            jMenu1.setText("Commands");
            jMenuItem1.setText("Refresh");
            jMenuItem2.setText("Back");
            jMenuItem1.addActionListener(new java.awt.event.ActionListener() 
            {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) 
                {
                    try {
                        jMenuItem1ActionPerformed1(evt);
                    } catch (UnknownHostException ex) {
                        Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Logger.getLogger(Message.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
                @Override
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jMenuItem1ActionPerformed2(evt);
                }
            });

            jMenu1.add(jMenuItem1);
            jMenu1.add(jMenuItem2);
            jMenuBar1.add(jMenu1);
            setJMenuBar(jMenuBar1);
            make();	
            setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
            mainText.setFont(new Font("Times New Roman", Font.ITALIC, 16));
	}
	void make()
	{
            System.out.println("make");
            javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
            getContentPane().setLayout(layout);
            layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 314, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 314, Short.MAX_VALUE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
            .addComponent(jScrollPane3))
            .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 565, Short.MAX_VALUE)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 565, Short.MAX_VALUE))
            .addGap(9, 9, 9)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
            
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING))
            .addContainerGap())
        );
        pack();
        setVisible(true);
            System.out.println("end of make");
    }
    private void nickListMouseClicked(java.awt.event.MouseEvent evt) throws UnknownHostException, IOException
    {
	if ((!nickList.getSelectedValue().equals(nick)))
	{
            String msg =  JOptionPane.showInputDialog(null, "Write text for private message: ");
			if (msg != null)
			{
                            Soc=new Socket("127.0.0.1",5217);
                            DataInputStream din;
                            DataOutputStream dout;
                            BufferedReader br;
                            din = new DataInputStream(Soc.getInputStream());
                            dout = new DataOutputStream(Soc.getOutputStream());
                            br = new BufferedReader(new InputStreamReader(System.in));
                            dout.writeUTF("MWRITE");
                            dout.writeUTF(nickList.getSelectedValue()+".txt");
                            //System.out.println("here....."+nickList.getSelectedValue());
                            dout.writeUTF(username+" says: "+msg);
			}
		 	System.out.println(nickList.getSelectedValue());
		}
	}
    private void jMenuItem1ActionPerformed1(java.awt.event.ActionEvent evt) throws UnknownHostException, IOException 
    {
        mainText.setText("");
    	read();
    }
    private void jMenuItem1ActionPerformed2(java.awt.event.ActionEvent evt) 
    {
    	setVisible(false);
        if(status.equals("Teacher"))
        {
            TeacherSignedIn teacher=new TeacherSignedIn(email,username);
            teacher.MAIN();
        }
        if(status.equals("Student"))
        {
            StudentSignedIn student= new StudentSignedIn(email,username);
            student.MAIN();
        }
    }


    public static String nick;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    public static javax.swing.JTextArea mainText;
     private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    static public javax.swing.JMenuItem jMenuItem1;
    static public javax.swing.JMenuItem jMenuItem2;
    static public javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JList nickList;

}
