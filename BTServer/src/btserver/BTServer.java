/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package btserver;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author binita
 */
public class BTServer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        ServerSocket soc=new ServerSocket(5217);
		System.out.println("FTP Server Started on Port Number 5217");
		while(true)
		{
			System.out.println("Waiting for Connection ...");
			transferfile t=new transferfile(soc.accept());

		}
    }
}
class transferfile extends Thread
{
	Socket ClientSoc;

	DataInputStream din;
	DataOutputStream dout;

	transferfile(Socket soc)
	{
		try
		{
			ClientSoc=soc;
			din=new DataInputStream(ClientSoc.getInputStream());
			dout=new DataOutputStream(ClientSoc.getOutputStream());
			System.out.println("FTP Client Connected ...");
			start();

		}
		catch(Exception ex)
		{
                    System.out.println("transfer file error: "+ex);
		}
	}
	void DownloadFile() throws Exception
	{
		String filename;
                filename=din.readUTF();
                String fn;
                fn=filename.substring(0,(filename.length())-1);
               // JOptionPane.showMessageDialog(null, fn);
                File fnew=new File(fn);
		if(fnew.exists())
		{
                    dout.writeUTF("READY");
                try (FileInputStream fin = new FileInputStream(fnew)) {
                    int ch;
                    do
                    {
                            ch=fin.read();
                            dout.writeUTF(String.valueOf(ch));
                    }
                    while(ch!=-1);
                }
			dout.writeUTF("File Downloaded Successfully");
			
		}
		else
		{
			dout.writeUTF("File Not Found");
			return;
		}
	}
        void UpdateFile()throws Exception
        {
            FileReader fin=new FileReader("nf.txt");
            BufferedReader buf=new BufferedReader(fin);
            String filename="",names="";
            while((filename=buf.readLine())!=null)
            {
              
                names+=(filename+"\r\n");
            }
              dout.writeUTF(names);
            fin.close();
            buf.close();
            
        }
        void Discussion()throws Exception
        {
            FileReader fin=new FileReader("discussion.txt");
            BufferedReader buf=new BufferedReader(fin);
            String filename="",names="";
            while((filename=buf.readLine())!=null)
            {
              
                names+=(filename+"\r\n");
            }
              dout.writeUTF(names);
            
            
        }
        void MRead() throws FileNotFoundException, IOException
        {
            String filename=din.readUTF();
            //JOptionPane.showMessageDialog(null, filename);
            File f;
                f = new File(filename);
                if(!f.exists())
                {
                boolean createNewFile = f.createNewFile();
                }
            FileReader fin=new FileReader(f);
            BufferedReader buf=new BufferedReader(fin);
            String fil;
            String nam="";
            while((fil=buf.readLine())!=null)
            {
              
                nam=nam+"\r\n"+fil;
                
            }
            dout.writeUTF(nam);

            //JOptionPane.showMessageDialog(null, nam);
              
        }
        void Post() throws IOException
        {
            String text=din.readUTF();
            FileWriter namefout;
                namefout = new FileWriter("discussion.txt",true);
                namefout.append(text);
                namefout.append("\r\n");
                namefout.close();
        }
        void MWrite() throws IOException
        {
            
            String filename=din.readUTF();
            String text=din.readUTF();
            FileWriter namefout;
                namefout = new FileWriter(filename,true);
                namefout.append(text);
                namefout.append("\r\n");
                namefout.close();
        }
	void UploadFile() throws Exception
	{
		String filename=din.readUTF();                
		if(filename.compareTo("File not found")==0)
		{
			return;
		}
                
		File f;
                f = new File(filename);
		String option;

		if(f.exists())
		{
			dout.writeUTF("File Already Exists");
			option=din.readUTF();
		}
		else
		{
			dout.writeUTF("SendFile");
			option="Y";
                        FileWriter namefout;
                namefout = new FileWriter("nf.txt",true);
                namefout.append(filename);
                namefout.append("\r\n");
                namefout.close();
		}

			if(option.compareTo("Y")==0)
			{
                try (FileOutputStream fout = new FileOutputStream(f)) {
                    int ch;
                    String temp;
                    do
                    {
                            temp=din.readUTF();
                            ch=Integer.parseInt(temp);
                            if(ch!=-1)
                            {
                                    fout.write(ch);
                            }
                    }while(ch!=-1);
                }
                
                
                        
				dout.writeUTF("File Uploaded Successfully");
			}
			
        }

	


    @Override
	public void run()
	{
		while(true)
		{
			try
			{
			System.out.println("Waiting for Command ...");
			String Command=din.readUTF();
			if(Command.compareTo("GET")==0)
			{
				System.out.println("\tDownload Command Received ...");
				DownloadFile();
				continue;
			}
			else if(Command.compareTo("SEND")==0)
			{
				System.out.println("\tUpload Command Receiced ...");
				UploadFile();
				continue;
			}
                        
                        else if(Command.compareTo("POST")==0)
			{
				System.out.println("\tPost Command Receiced ...");
				Post();
				continue;
			}
                        else if(Command.compareTo("UPDATE")==0)
			{
				System.out.println("\tUpdate Command Receiced ...");
				UpdateFile();
				continue;
			}
                        else if(Command.compareTo("DISCUSSION")==0)
			{
				System.out.println("\tDiscussion Command Receiced ...");
				Discussion();
				continue;
			}
                        else if(Command.compareTo("MREAD")==0)
			{
				System.out.println("\tmessage read Command Receiced ...");
				MRead();
				continue;
			}
                        else if(Command.compareTo("MWRITE")==0)
			{
				System.out.println("\tmessage write Command Receiced ...");
				MWrite();
				continue;
			}
			else if(Command.compareTo("DISCONNECT")==0)
			{
                            din.close();
                        dout.close();
                        ClientSoc.close();
				System.out.println("\tDisconnect Command Received ...");
				System.exit(1);
			}
			}
			catch(Exception ex)
			{
                            System.out.println("run error: "+ex);
			}
		}
	}
}